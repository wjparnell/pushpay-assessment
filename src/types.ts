export interface PersonType {
	name: string,
	species: string,
	films: string[],
	height: string,
	mass: string,
	birth_year: string,
	gender: string,
}

export interface FilmType {
	title: string,
	url: string
}

export interface SpeciesType {
	name: string,
	url: string
}
