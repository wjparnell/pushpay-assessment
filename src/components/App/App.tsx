import React from 'react';
import People from '../People';
import './App.css';

function App() {
	// Shows title and info text, then renders all characters separately
	return (
		<React.Fragment>
			<div class="container">
				<h1 class="title">The Star Wars Character Database!</h1>
				<p style={{"text-align": "center", fontSize: "24px"}}>Find and search for information on all the characters present in the Star Wars film universe! Character data taken from SWAPI.</p>
				<h6 style={{"text-align": "center"}}>*Does not include Sequel Trilogy films, as they are not canon (at least they shouldn't be).</h6>
			</div>
			<People />
		</React.Fragment>
	);
}

export default App;
