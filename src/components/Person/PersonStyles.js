// CSS styles used in Person object

// Style for collapsed people listing
export const personStyle = {
	borderColor: "gold",
	borderStyle: "solid",
	marginTop: "10px",
	padding: "10px",
	textAlign: "center",
};

// Style for expanded details view
export const infoBoxStyle = {
	borderColor: "gold",
	borderStyle: "solid",
	borderTop: "none",
	display: "grid",
	gridColumnGap: "5px",
	gridTemplateColumns: "50% 50%",
	marginBottom: "none",
	padding: "10px",
};

// Style for a single column in the details view
export const columnStyle = {
	width: "50%",
};

// Removes top margin for header text
export const noTopMargin = {
	marginTop: 0,
};
