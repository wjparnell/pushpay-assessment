import React from 'react';
import { PersonType } from '../../types';
import { personStyle, infoBoxStyle, columnStyle, noTopMargin } from './PersonStyles.js';

interface PersonProps {
	person: PersonType
}

function Person({ person }: PersonProps, props: any) {

	// Controls whether the detail view is open
	const [open, setOpen] = React.useState(false);
	// Toggles the detail view
	const clickToggle = () => setOpen(!open);

	// Clicking on the person's name shows more details, included here
	const InfoWindow = () => {
		return (
			<React.Fragment>
				<div style={infoBoxStyle}>
					<div style={columnStyle}>
						<h1 style={noTopMargin}>{person.name}</h1>
						<em>{person.species}</em><br/><br/>
						<strong>Sex:&nbsp;</strong>{person.gender[0].toUpperCase() + person.gender.substring(1)}<br/>
						<strong>Born:&nbsp;</strong>{person.birth_year[0].toUpperCase() + person.birth_year.substring(1)}<br/>
						<strong>Height:&nbsp;</strong>{person.height}&nbsp;cm<br/>
						<strong>Weight:&nbsp;</strong>{person.mass}&nbsp;kg<br/>
					</div>
					<div style={columnStyle}>
						<h2 style={noTopMargin}>Appears in:</h2>
						{person.films.map(film => <li>{film}</li>)}
					</div>
				</div>
			</React.Fragment>
		);
	}

	// An Person object contains the character's name, which when clicked, will show more details in an InfoWindow, which is controlled by the open state variable
	return (
		<React.Fragment>
			<div style={personStyle} onClick={clickToggle}>
				<strong>{person.name}</strong>
			</div>
			{open ? <InfoWindow /> : null}
		</React.Fragment>
	);
}

export default Person
