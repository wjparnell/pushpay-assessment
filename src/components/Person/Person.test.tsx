import { render, screen, fireEvent } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import { PersonType } from '../../types';
import Person from './Person'

describe('<Person />', () => {
	// Sample person object
	const person: PersonType = {
		name: 'Darth Vader',
		species: 'Human',
		height: "202 cm",
		mass: "136 kg",
		birth_year: "41.9BBY",
		gender: "Male",
		films: ["A New Hope", "The Empire Strikes Back", "Return of the Jedi", "Revenge of the Sith"],
	};
	test('should render the person\'s name', () => {
		render(<Person person={person} />);

		screen.getByText(person.name);
	});

	test('should render the infobox once person is clicked', async () => {
		render(<Person person={person} />);
		await userEvent.click(screen.getByText(person.name));
		screen.getByText(person.species);
	});

});
