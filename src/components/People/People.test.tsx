import { render, screen, fireEvent, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import { PersonType } from '../../types';
import People from './People'

describe('<People />', () => {
	test('should render all possible characters', async () => {
		render(<People />);
		// Entries won't be rendered immediately when loading
		expect(screen.queryByText("Luke Skywalker")).toBeNull();
		// Wait for API calls to finish
		await waitFor(() => {
			// Check the first and last entries, they should both be rendered
			expect(screen.getByText("Luke Skywalker")).toBeInTheDocument();
			expect(screen.getByText("Tion Medon")).toBeInTheDocument();
		}, {timeout: 5000});
	});

	test('should only render characters matching the search', async () => {
		render(<People />);
		// Wait for API calls to finish
		await waitFor(() => {
			// Ensure both entries are listed
			expect(screen.getByText("Darth Vader")).toBeInTheDocument();
			expect(screen.getByText("Anakin Skywalker")).toBeInTheDocument();
		}, {timeout: 5000});
		// Type in the search box
		await userEvent.type(screen.getByRole('textbox'), 'darth');
		// This entry should still be listed
		expect(screen.getByText("Darth Vader")).toBeInTheDocument();
		// This entry shouldn't still be listed
		expect(screen.queryByText("Anakin Skywalker")).toBeNull();
	});
});
