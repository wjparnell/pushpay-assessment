// Centered container for the list of Persons
export const container = {
	margin: "auto",
	width: "75%",
};

// Style for the search box
export const searchBoxStyle = {
	boxSizing: "border-box",
	margin: "5px",
	marginLeft: "25%",
	padding: "10px",
	paddingLeft: "10px",
	width: "50%",
};
