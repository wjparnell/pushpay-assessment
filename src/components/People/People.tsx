import React from 'react'

import { fetchJson, fetchAll } from '../../api'
import { PersonType, SpeciesType, FilmType } from '../../types'
import Person from '../Person'
import { container, searchBoxStyle } from './PeopleStyles'

function People() {
	// Lists of characters, films, and species
	const [people, setPeople] = React.useState<PersonType[]>([]);
	const [films, setFilms] = React.useState<FilmType[]>([]);
	const [species, setSpecies] = React.useState<SpeciesType[]>([]);

	// Text currently in the search box
	const [query, setQuery] = React.useState("");

	// Fetch everything from the films and species lists from the API so lookups can be done locally to match characters to their films and species
	React.useEffect(() => {
		fetchAll<FilmType[]>('films')
		.then(filmResponse => {
			setFilms(filmResponse);
		})
	}, []);
	React.useEffect(() => {
		fetchAll<SpeciesType[]>('species')
		.then(speciesResponse => {
			setSpecies(speciesResponse);
		})
	}, []);

	// Fetch the list of people
	React.useEffect(() => {
		fetchAll<PersonType[]>('people')
		.then(peopleResponse => {
			// Change the urls in the species and films properties to text
			for (let person of peopleResponse) {
				// Find the species object that contains the url specified in the person object
				const spec = species.find(elem => elem.url === person.species[0]);
				// The species value of humans are all set to [] in the API when it should be species/1, so this must be hardcoded
				if (spec === undefined) {
					person.species = "Human";
				}
				else {
					person.species = spec.name;
				}
				// Same process for films, but must now loop through and change all films that a person was in
				for (let i = 0; i < person.films.length; i++) {
					const mov = films.find(elem => elem.url === person.films[i]);
					if (mov !== undefined) {
						person.films[i] = mov.title;
					}
				}
			}
			setPeople(peopleResponse);
		})
	}, [films, species]); // Re-run when either other list has been fetched so that the new data is accurate

	// Whenever the text in the search box is changed, grab the DOM event, then extract the value from the search box, then set the query state variable
	const modifyQuery = (event) => {
		setQuery(event.target.value);
	}

	// Render a Person object for all characters whose names match the text in the search box
	return (
		<React.Fragment>
			<div style={container}>
				<input type="text" placeholder="Search..." style={searchBoxStyle} onChange={modifyQuery}/>
				{people.filter(person => person.name.toLowerCase().includes(query.toLowerCase()))
				.map(person => <Person person={person} />)}
			</div>
		</React.Fragment>
	)
}

export default People
