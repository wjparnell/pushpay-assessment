// Fetches a single JSON object from the API
export async function fetchJson<Response = any>(url: string, init?: RequestInit): Promise<Response> {
	const response = await fetch(url, {
		...init ?? {},
		headers: {
			Accept: 'application/json',
			'Content-Type': 'application/json',
		}
	});
	return response.json();
}

// Returns a list of all objects from a single category in the API
export async function fetchAll<Response = any>(data: string) {
	// Use fetchJson function to do the fetching and converting to a JSON object
	let response = await fetchJson(`https://swapi.dev/api/${data}`);
	// Create a list to concatenate all the results into one object
	let list: any[] = response.results;
	// The url of the next page of data is stored in next, so fetch every subsequent page until there's nothing more to fetch
	while (response.next !== null) {
		response = await fetchJson(response.next);
		list = list.concat(response.results);
	}
	return list;
}
